# Csver

Library to work with CSV files.

## Usage

Read a single or multiple files:

```csharp
Csv csv = new Csv();
csv.Read("./file.csv");
csv.Read("./another.csv");
```

Get values:

```csharp
IReadOnlyList<string> Columns = csv.Columns;
IReadOnlyList<string> Rows = csv.Rows;
string value = csv["Column", "Row"];
```

Set values:

```csharp
csv.ColumnAdd("ColumnX");
csv.RowAdd("RowX");
csv["ColumnX", "RowX"] = "new value";
csv["ColumnNew", "RowNew"] = "ValueNew";
```

Write to a file:

```csharp
csv.Write("./file.csv");
```

## License

Library is available under the MIT license.

Repository icon is from https://ikonate.com/ pack and is used under the MIT license.