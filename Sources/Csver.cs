﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Iodynis.Libraries.Csving
{
    /// <summary>
    /// Provides CSV-files manipulation functionality.
    /// </summary>
    public class Csv
    {
        #region Definitions
        /// <summary>
        /// Determines what to do if rows with the same names are met.
        /// </summary>
        public enum OnCollisionBehavior
        {
            /// <summary>
            /// Keep the old value.
            /// </summary>
            KEEP      = 1,
            /// <summary>
            /// Overwrite the old value with the new one.
            /// </summary>
            OVERWRITE = 2,
            /// <summary>
            /// Throw an Exception
            /// </summary>
            PANIC     = 3,
        }
        /// <summary>
        /// Determines what to do if whitespace symbols or unicode sequences (the \uXXXX ones) are encountered.
        /// </summary>
        [Flags]
        public enum ReplaceCharacterEnum
        {
            /// <summary>
            /// Treat all symbols as is.
            /// </summary>
            NONE = 0,
            /// <summary>
            /// Replace \t with the tab characters.
            /// </summary>
            TAB = 1,
            /// <summary>
            /// Replace \r with the return character.
            /// </summary>
            RETURN = 2,
            /// <summary>
            /// Replace \n with the new line character.
            /// </summary>
            NEW_LINE = 4,
            /// <summary>
            /// Replace \uXXXX with the corresponding unicode character.
            /// </summary>
            UNICODE = 8,
            /// <summary>
            /// Replace \t, \r, \n and \uXXXX with the tab, return, new line and corresponding unicode character accordingly.
            /// </summary>
            ALL = 15,
        }
        private Tuple<string, string>[] ReplacesOnReadArray;
        private Tuple<string, string>[] ReplacesOnWriteArray;
        private bool ReplaceUnicode = false;

        private bool IsColumnFirst { get; }
        /// <summary>
        /// The name of the first column that contains all the rows keys.
        /// </summary>
        public string KeyColumnName { get; set; }

        private Dictionary<string, Dictionary<string, string>> ColumnToRowToValue { get; set; } = new Dictionary<string, Dictionary<string, string>>();
        private Dictionary<string, Dictionary<string, string>> RowToColumnToValue { get; set; } = new Dictionary<string, Dictionary<string, string>>();

        private List<string> _Columns = new List<string>();
        /// <summary>
        /// Readonly list of all the columns names.
        /// </summary>
        public IReadOnlyList<string> Columns
        {
            get
            {
                return _Columns;
            }
        }
        private List<string> _Rows = new List<string>();
        /// <summary>
        /// Readonly list of all the rows keys.
        /// </summary>
        public IReadOnlyList<string> Rows
        {
            get
            {
                return _Rows;
            }
        }
        /// <summary>
        /// Get the string by to indexes.
        /// </summary>
        /// <param name="column">Column index.</param>
        /// <param name="row">Row index.</param>
        /// <returns>The string.</returns>
        public string this[string column, string row]
        {
            get
            {
                if (!ColumnToRowToValue.ContainsKey(column))
                {
                    throw new Exception($"Column {column} does not exist.");
                }
                if (!RowToColumnToValue.ContainsKey(row))
                {
                    throw new Exception($"Row {row} does not exist.");
                }
                return ColumnToRowToValue[column][row];
            }
            set
            {
                if (!ColumnExists(column))
                {
                    ColumnAdd(column);
                }
                if (!RowExists(row))
                {
                    RowAdd(row);
                }
                ColumnToRowToValue[column][row] = value;
                RowToColumnToValue[row][column] = value;
                //if (!ColumnToRowToValue.ContainsKey(column))
                //{
                //    ColumnToRowToValue.Add(column, new Dictionary<string, string>());
                //    foreach (string rowExisting in RowToColumnToValue.Keys)
                //    {
                //        ColumnToRowToValue[column].Add(rowExisting, null);
                //    }
                //}
                //if (!ColumnToRowToValue[column].ContainsKey(row))
                //{
                //    ColumnToRowToValue[column].Add(row, value);
                //}
                //else
                //{
                //    ColumnToRowToValue[column][row] = value;
                //}
                //if (!RowToColumnToValue.ContainsKey(row))
                //{
                //    RowToColumnToValue.Add(row, new Dictionary<string, string>());
                //    foreach (string columnExisting in ColumnToRowToValue.Keys)
                //    {
                //        RowToColumnToValue[row].Add(columnExisting, null);
                //    }
                //}
                //if (!RowToColumnToValue[row].ContainsKey(column))
                //{
                //    RowToColumnToValue[row].Add(column, value);
                //}
                //else
                //{
                //    RowToColumnToValue[row][column] = value;
                //}
            }
        }
        /// <summary>
        /// Try to get the value for specific column and row.
        /// </summary>
        /// <param name="column">The column name.</param>
        /// <param name="row">The row name.</param>
        /// <param name="value">The value for the specified column and row or null if there is no such column or row.</param>
        /// <returns></returns>
        public bool TryGetValue(string column, string row, out string value)
        {
            if (!ColumnToRowToValue.ContainsKey(column))
            {
                value = null;
                return false;
            }
            if (!RowToColumnToValue.ContainsKey(row))
            {
                value = null;
                return false;
            }

            value = ColumnToRowToValue[column][row];
            return true;
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Create an empty instance.
        /// </summary>
        /// <param name="isColumnFirst">True for csv[column][row] usage. False for csv[row][column] usage.</param>
        public Csv(bool isColumnFirst = true)
            : this(isColumnFirst, ReplaceCharacterEnum.NONE) { }

        /// <summary>
        /// Create an empty instance.
        /// </summary>
        /// <param name="replaces">Substrings to replace (usually \t, \r and \n symbols).</param>
        public Csv(ReplaceCharacterEnum replaces)
            : this(true, replaces) { }

        /// <summary>
        /// Create an empty instance.
        /// </summary>
        /// <param name="replaces">Substrings to replace (usually \t, \r and \n symbols).</param>
        /// <param name="replaceUnicode">If replace the encountered unicode sequences (the \uXXXX ones).</param>
        public Csv(Dictionary<string, string> replaces, bool replaceUnicode = false)
            : this(true, replaces, replaceUnicode) { }

        /// <summary>
        /// Create an empty instance.
        /// </summary>
        /// <param name="isColumnFirst">True for csv[column][row] usage. False for csv[row][column] usage.</param>
        /// <param name="replaces">Substrings to replace (usually \t, \r and \n symbols).</param>
        public Csv(bool isColumnFirst, ReplaceCharacterEnum replaces)
        {
            IsColumnFirst = isColumnFirst;

            Init(replaces);
        }

        /// <summary>
        /// Create an empty instance.
        /// </summary>
        /// <param name="isColumnFirst">True for csv[column][row] usage. False for csv[row][column] usage.</param>
        /// <param name="replaces">Substrings to replace (usually \t, \r and \n symbols).</param>
        /// <param name="replaceUnicode">If replace the encountered unicode sequences (the \uXXXX ones).</param>
        public Csv(bool isColumnFirst, Dictionary<string, string> replaces, bool replaceUnicode = false)
        {
            IsColumnFirst = isColumnFirst;

            Init(replaces, replaceUnicode);
        }

        /// <summary>
        /// Create an empty instance.
        /// </summary>
        /// <param name="csv">Csv to clone.</param>
        private Csv(Csv csv)
        {
            IsColumnFirst = csv.IsColumnFirst;
            ReplacesOnReadArray = (Tuple<string, string>[])csv.ReplacesOnReadArray.Clone();
            ReplacesOnWriteArray = (Tuple<string, string>[])csv.ReplacesOnWriteArray.Clone();
            ReplaceUnicode = csv.ReplaceUnicode;

            _Rows = new List<string>(csv._Rows);
            _Columns = new List<string>(csv._Columns);
        }

        ///// <summary>
        ///// Create from a CSV file.
        ///// </summary>
        ///// <param name="pathOrText">Path to the file or file contents. Distinction is made by the presence of \r and \n characters.</param>
        ///// <param name="isColumnFirst">Should columns come before rows when using the index syntax [,]?</param>
        ///// <param name="replaces">Substrings to replace (usually \t, \r and \n symbols).</param>
        //public Csv(string pathOrText, bool isColumnFirst = true, ReplaceCharacterEnum replaces = ReplaceCharacterEnum.NONE)
        //{
        //    IsColumnFirst = isColumnFirst;
        //    Dictionary<string, Dictionary<string, string>> dictionary;
        //    string columnFirstName;
        //    if (pathOrText.Contains('\n') || pathOrText.Contains('\r'))
        //    {
        //        dictionary = ReadString(pathOrText, out columnFirstName);
        //    }
        //    else
        //    {
        //        dictionary = ReadFile(pathOrText, out columnFirstName);
        //    }
        //    ColumnFirstName = columnFirstName;

        //    Init(dictionary, replaces);
        //}
        ///// <summary>
        ///// Create from a CSV file.
        ///// </summary>
        ///// <param name="pathOrText">Path to the file or file contents. Distinction is made by the presence of \r and \n characters.</param>
        ///// <param name="isColumnFirst">Should columns come before rows when using the index syntax [,]?</param>
        ///// <param name="replaces">Substrings to replace (usually \t, \r and \n symbols).</param>
        //public Csv(string pathOrText, bool isColumnFirst = true, Dictionary<string, string> replaces = null, bool replaceUnicode = false)
        //{
        //    IsColumnFirst = isColumnFirst;
        //    Dictionary<string, Dictionary<string, string>> dictionary;
        //    string columnFirstName;
        //    if (pathOrText.Contains('\n') || pathOrText.Contains('\r'))
        //    {
        //        dictionary = ReadString(pathOrText, out columnFirstName);
        //    }
        //    else
        //    {
        //        dictionary = ReadFile(pathOrText, out columnFirstName);
        //    }
        //    ColumnFirstName = columnFirstName;

        //    Init(dictionary, replaces, replaceUnicode);
        //}
        ///// <summary>
        ///// Create from a dictionary provided.
        ///// </summary>
        ///// <param name="dictionary">Dictionary to build upon.</param>
        ///// <param name="isColumnFirst">Should columns come before rows when using the index syntax [,]?</param>
        ///// <param name="replaces">Substrings to replace (usually \t, \r and \n symbols).</param>
        //public Csv(Dictionary<string, Dictionary<string, string>> dictionary,
        //    bool isColumnFirst = true, Dictionary<string, string> replaces = null, bool replaceUnicode = false)
        //{
        //    IsColumnFirst = isColumnFirst;

        //    Init(replaces, replaceUnicode);
        //    Read(dictionary);
        //}

        private void Init(ReplaceCharacterEnum replaceCharacters)
        {
            Dictionary<string, string> replaces = new Dictionary<string, string>();

            if (replaceCharacters.HasFlag(ReplaceCharacterEnum.TAB))
            {
                replaces.Add("\\t", "\t");
            }
            if (replaceCharacters.HasFlag(ReplaceCharacterEnum.RETURN))
            {
                replaces.Add("\\r", "\r");
            }
            if (replaceCharacters.HasFlag(ReplaceCharacterEnum.NEW_LINE))
            {
                replaces.Add("\\n", "\n");
            }
            bool replaceUnicode = replaceCharacters.HasFlag(ReplaceCharacterEnum.UNICODE);

            Init(replaces, replaceUnicode);
        }
        private void Init(Dictionary<string, string> replaces, bool replaceUnicode)
        {
            replaces = replaces ?? new Dictionary<string, string>();

            ReplaceUnicode = replaceUnicode;
            // Do not use the user provided dictionary -- create a new one
            Dictionary<string, string> ReplacesOnRead = new Dictionary<string, string>(replaces);
            // Create the inverted one
            Dictionary<string, string> ReplacesOnWrite = Invert(ReplacesOnRead);

            {
                var invalidKeys = ReplacesOnRead.Keys.Where(_key => _key.Contains('"'));
                if (invalidKeys.Count() > 0)
                {
                    throw new ArgumentException($"Keys cannot contain \" character. Sequence \"\" is replaced with \" on reading and vice versa on writing. This behavior is part of the CSV format and cannot be redefined.");
                }
                var invalidValues = ReplacesOnRead.Values.Where(_key => _key.Contains('"'));
                if (invalidValues.Count() > 0)
                {
                    throw new ArgumentException($"Values cannot contain \" character. Sequence \"\" is replaced with \" on reading and vice versa on writing. This behavior is part of the CSV format and cannot be redefined.");
                }
            }

            ReplacesOnRead.Add("\"\"", "\"");
            ReplacesOnWrite.Add("\"", "\"\"");
            ReplacesOnReadArray = ReplacesOnRead.Select(_pair => new Tuple<string, string>(_pair.Key, _pair.Value)).ToArray();
            ReplacesOnWriteArray = ReplacesOnWrite.Select(_pair => new Tuple<string, string>(_pair.Key, _pair.Value)).ToArray();
        }
        #endregion

        #region Read and write
        /// <summary>
        /// Read a file or a string. Distinction is made by the presence of \r and \n symbols.
        /// </summary>
        /// <param name="pathOrText">Path to the file or file contents. Distinction is made by the presence of \r and \n characters.</param>
        public void Read(string pathOrText)
        {
            Read(pathOrText, ReplacesOnReadArray, ReplaceUnicode);
        }
        /// <summary>
        /// Read a file or a string. Distinction is made by the presence of \r and \n symbols.
        /// </summary>
        /// <param name="pathOrText">Path to the file or file contents. Distinction is made by the presence of \r and \n characters.</param>
        /// <param name="replaces">Substrings to replace (usually \t, \r and \n symbols).</param>
        public void Read(string pathOrText, Dictionary<string, string> replaces)
        {
            Read(pathOrText, replaces, ReplaceUnicode);
        }
        /// <summary>
        /// Read a file or a string. Distinction is made by the presence of \r and \n symbols.
        /// </summary>
        /// <param name="pathOrText">Path to the file or file contents. Distinction is made by the presence of \r and \n characters.</param>
        /// <param name="replaces">Substrings to replace (usually \t, \r and \n symbols).</param>
        /// <param name="replaceUnicode"></param>
        public void Read(string pathOrText, Dictionary<string, string> replaces, bool replaceUnicode)
        {
            replaces = replaces ?? new Dictionary<string, string>();
            {
                var invalidKeys = replaces.Keys.Where(_key => _key.Contains('"'));
                if (invalidKeys.Count() > 0)
                {
                    throw new ArgumentException($"Keys cannot contain \" character. Sequence \"\" is replaced with \" on reading and vice versa on writing. This behavior is part of the CSV format and cannot be redefined.");
                }
                var invalidValues = replaces.Values.Where(_key => _key.Contains('"'));
                if (invalidValues.Count() > 0)
                {
                    throw new ArgumentException($"Values cannot contain \" character. Sequence \"\" is replaced with \" on reading and vice versa on writing. This behavior is part of the CSV format and cannot be redefined.");
                }
            }

            replaces.Add("\"\"", "\"");
            Tuple<string, string>[] replacesArray = replaces.Select(_pair => new Tuple<string, string>(_pair.Key, _pair.Value)).ToArray();
            Read(pathOrText, replacesArray, replaceUnicode);
        }
        private void Read(string pathOrText, Tuple<string, string>[] replaces, bool replaceUnicode)
        {
            Dictionary<string, Dictionary<string, string>> dictionary;
            if (pathOrText.Contains('\n') || pathOrText.Contains('\r'))
            {
                dictionary = ReadString(pathOrText, replaces, replaceUnicode, out string key);
                KeyColumnName = key;
            }
            else
            {
                dictionary = ReadFile(pathOrText, replaces, replaceUnicode, out string key);
                KeyColumnName = key;
            }
            Read(dictionary);
        }
        // TODO: OnCollisionBehavior
        /// <summary>
        /// Merge the specified dictionary.
        /// </summary>
        /// <param name="dictionary">The dictionary to merge</param>
        public void Read(Dictionary<string, Dictionary<string, string>> dictionary)
        {
            Dictionary<string, Dictionary<string, string>> dictionaryMerged = (IsColumnFirst ? ColumnToRowToValue : RowToColumnToValue) ?? new Dictionary<string, Dictionary<string, string>>();
            dictionary = dictionary ?? new Dictionary<string, Dictionary<string, string>>();

            // Merge dictionaries
            {
                foreach (var columnAndRowToValue in dictionary)
                {
                    if (!dictionaryMerged.TryGetValue(columnAndRowToValue.Key, out Dictionary<string, string> rowToValue))
                    {
                        rowToValue = new Dictionary<string, string>();
                        dictionaryMerged[columnAndRowToValue.Key] = rowToValue;
                    }
                    foreach (var rowAndValue in columnAndRowToValue.Value)
                    {
                        rowToValue[rowAndValue.Key] = rowAndValue.Value;
                    }
                }
            }

            // Fill the transpositioned dictionary
            Dictionary<string, Dictionary<string, string>> dictionaryTranspositioned = new Dictionary<string, Dictionary<string, string>>();
            if (dictionary.Count != 0)
            {
                foreach (string row in dictionary.ElementAt(0).Value.Keys)
                {
                    dictionaryTranspositioned.Add(row, new Dictionary<string, string>());
                    foreach (string column in dictionary.Keys)
                    {
                        dictionaryTranspositioned[row].Add(column, dictionary[column][row]);
                    }
                }
            }

            // (Re)assign straight and transpositioned dictionaries appropriately
            ColumnToRowToValue = IsColumnFirst ? dictionary : dictionaryTranspositioned;
            RowToColumnToValue = IsColumnFirst ? dictionaryTranspositioned : dictionary;

            // (Re)build cache
            //ColumnsChanged = true;
            //RowsChanged = true;
            _Columns = ColumnToRowToValue.Keys.ToList();
            _Rows = RowToColumnToValue.Keys.ToList();
        }
        /// <summary>
        /// Merge multiple Csvs into this one.
        /// </summary>
        /// <param name="csvs">Csvs to merge into this one.</param>
        public void Read(params Csv[] csvs)
        {
            Read(OnCollisionBehavior.OVERWRITE, csvs);
        }
        /// <summary>
        /// Merge multiple Csvs into this one.
        /// </summary>
        /// <param name="csvs">Csvs to merge into this one.</param>
        public void Read(IEnumerable<Csv> csvs)
        {
            Read(OnCollisionBehavior.OVERWRITE, csvs);
        }
        /// <summary>
        /// Merge multiple Csvs into this one.
        /// </summary>
        /// <param name="onCollisionBehavior">What to do if a duplicate row is found</param>
        /// <param name="csvs">Csvs to merge into this one.</param>
        public void Read(OnCollisionBehavior onCollisionBehavior, params Csv[] csvs)
        {
            Read(onCollisionBehavior, csvs);
        }
        /// <summary>
        /// Merge multiple Csvs into this one.
        /// </summary>
        /// <param name="onCollisionBehavior">What to do if a duplicate row is found</param>
        /// <param name="csvs">Csvs to merge into this one.</param>
        public void Read(OnCollisionBehavior onCollisionBehavior, IEnumerable<Csv> csvs)
        {
            // Run through every csv
            foreach (Csv csv in csvs)
            {
                // Through every dictionary
                foreach (KeyValuePair<string, Dictionary<string, string>> keyAndDictionary in csv.ColumnToRowToValue)
                {
                    // Maybe key is not defined yet, then just copy and go on
                    if (!ColumnToRowToValue.ContainsKey(keyAndDictionary.Key))
                    {
                        ColumnToRowToValue.Add(keyAndDictionary.Key, new Dictionary<string, string>(keyAndDictionary.Value));
                        continue;
                    }
                    // Key already exists. Merge dictionaries under that key
                    foreach (KeyValuePair<string, string> variableAndValue in keyAndDictionary.Value)
                    {
                        // Check if variable is already defined
                        if (ColumnToRowToValue[keyAndDictionary.Key].ContainsKey(variableAndValue.Key))
                        {
                            switch (onCollisionBehavior)
                            {
                                case OnCollisionBehavior.KEEP:
                                    break;
                                case OnCollisionBehavior.OVERWRITE:
                                    ColumnToRowToValue[keyAndDictionary.Key][variableAndValue.Key] = variableAndValue.Value;
                                    break;
                                case OnCollisionBehavior.PANIC:
                                    throw new Exception($"Variable {variableAndValue.Key} is already defined.");
                                default:
                                    throw new Exception($"Merge way {onCollisionBehavior} is not supported.");
                            }
                        }
                        // Add a new variable under the specified key
                        else
                        {
                            ColumnToRowToValue[keyAndDictionary.Key].Add(variableAndValue.Key, variableAndValue.Value);
                        }
                    }
                }
            }
        }

        private static Dictionary<string, Dictionary<string, string>> ReadFile(string path, Tuple<string, string>[] replaces, bool replaceUnicode, out string key)
        {
            string text = "";
            using (FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite, 4096, FileOptions.SequentialScan))
            using (StreamReader streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                text = streamReader.ReadToEnd();
            }

            return ReadString(text, replaces, replaceUnicode, out key);
        }
        private static Dictionary<string, Dictionary<string, string>> ReadString(string @string, bool replaceUnicode, out string key)
        {
            return ReadString(@string, null, replaceUnicode, out key);
        }
        // TODO: Deal with replaceUnicode argument
        private static Dictionary<string, Dictionary<string, string>> ReadString(string @string, Tuple<string, string>[] replaces, bool replaceUnicode, out string key)
        {
            if (@string == null)
            {
                throw new ArgumentNullException("Provided text is null.");
            }

            Dictionary<string, Dictionary<string, string>> dictionary = new Dictionary<string, Dictionary<string, string>>();

            List<List<string>> rows = Linize(@string);
            if (rows.Count == 0)
            {
                key = null;
                return dictionary;
            }
            if (rows[0].Count == 0)
            {
                key = null;
                return dictionary;
            }

            key = rows[0][0];
            List<string> columnKeys = rows[0].Select(_column => _column).ToList();
            Dictionary<string, string>[] columns = new Dictionary<string, string>[columnKeys.Count];

            bool noReplacesNeeded = replaces == null || replaces.Length == 0;
            for (int columnIndex = 1; columnIndex < columnKeys.Count; columnIndex++)
            {
                string columnKey = columnKeys[columnIndex];
                Dictionary<string, string> column = new Dictionary<string, string>();
                columns[columnIndex] = column;
                dictionary.Add(columnKey, column);

                for (int rowIndex = 1; rowIndex < rows.Count; rowIndex++)
                {
                    List<string> row = rows[rowIndex];
                    column[row[0]] = row[columnIndex] == null ? null : noReplacesNeeded ? row[columnIndex] : Replace(row[columnIndex], replaces);
                }
            }

            //for (int linesIndex = 1; linesIndex < lines.Count; linesIndex++)
            //{
            //    List<string> line = lines[linesIndex];

            //    for (int columnIndex = 1; columnIndex < columnKeys.Count; columnIndex++)
            //    {
            //        columns[columnIndex][line[0]] = CsvParseColumnsReplace(line[columnIndex], replaces);
            //    }
            //}

            return dictionary;
        }
        /// <summary>
        /// Write the data to a CSV file.
        /// </summary>
        /// <param name="path">The path for the file.</param>
        public void Write(string path)
        {
            File.WriteAllText(path, Print());
        }
        /// <summary>
        /// Write the data for specific columns and rows to a CSV file.
        /// </summary>
        /// <param name="path">The path for the file.</param>
        /// <param name="columns">The columns to write.</param>
        /// <param name="rows">The rows to write.</param>
        public void Write(string path, IEnumerable<string> columns, IEnumerable<string> rows)
        {
            File.WriteAllText(path, Print(columns, rows));
        }
        /// <summary>
        /// Write the data to a string.
        /// </summary>
        public string Print()
        {
            return Print(null, null);
        }
        /// <summary>
        /// Write the data to a string.
        /// </summary>
        /// <param name="columns">The columns to write.</param>
        /// <param name="rows">The rows to write.</param>
        public string Print(IEnumerable<string> columns, IEnumerable<string> rows)
        {
            IEnumerable<string> rowNames = rows ?? _Rows;
            IEnumerable<string> columnNames = columns ?? _Columns;

            StringBuilder stringBuilder = new StringBuilder();

            // Fill the title
            stringBuilder.Append("\"");
            stringBuilder.Append(KeyColumnName);
            stringBuilder.Append("\"");

            foreach (string column in columnNames)
            {
                stringBuilder.Append(",\"");
                stringBuilder.Append(column);
                stringBuilder.Append("\"");
            }
            stringBuilder.Append("\r\n");

            // Fill the rows
            foreach (string rowName in rowNames)
            {
                stringBuilder.Append("\"");
                stringBuilder.Append(rowName);
                stringBuilder.Append("\"");

                foreach (string columnName in columnNames)
                {
                    string value = ColumnToRowToValue[columnName][rowName];

                    if (value == null)
                    {
                        stringBuilder.Append(",");
                    }
                    else if (value == "")
                    {
                        stringBuilder.Append(",\"\"");
                    }
                    else
                    {
                        //// Check for characters that need quotes
                        //bool areQuotesNeeded = new List<char>() { ',', '"' }.Any(lambda_quotableChar => value.Contains(lambda_quotableChar));

                        //// Endouble quotes
                        //value = value.Replace("\"", "\"\"");

                        value = Replace(value, ReplacesOnWriteArray);

                        //// If escapes are used
                        //if (EscapedCharacters != 0)
                        //{
                        //    value = value.Replace("\\", "\\\\");
                        //}
                        //// Escape tabs
                        //if ((EscapedCharacters & ReplaceCharacterEnum.TAB) != 0)
                        //{
                        //    value = value.Replace("\t", "\\t");
                        //}
                        //// Escape new lines
                        //if ((EscapedCharacters & ReplaceCharacterEnum.NEW_LINE) != 0)
                        //{
                        //    value = value.Replace("\r", "\\r");
                        //    value = value.Replace("\n", "\\n");
                        //}

                        //stringBuilder.Append(",");
                        //if (areQuotesNeeded)
                        //{
                        //    stringBuilder.Append('"');
                        //}
                        //stringBuilder.Append(value);
                        //if (areQuotesNeeded)
                        //{
                        //    stringBuilder.Append('"');
                        //}

                        stringBuilder.Append(",\"");
                        stringBuilder.Append(value);
                        stringBuilder.Append("\"");
                    }
                }
                stringBuilder.Append("\r\n");
            }

            return stringBuilder.ToString();
        }
        #endregion

        #region Columns and rows
        /// <summary>
        /// Check if a column exists.
        /// </summary>
        /// <param name="name">Name of the column.</param>
        /// <returns>If the column with specified name exists.</returns>
        public bool ColumnExists(string name)
        {
            return ColumnToRowToValue.ContainsKey(name);
        }
        /// <summary>
        /// Add a column.
        /// </summary>
        /// <param name="name">Name of the column to add.</param>
        public void ColumnAdd(string name)
        {
            if (ColumnExists(name))
            {
                return;
            }

            // Row-Column-Value
            foreach (string row in _Rows)
            {
                RowToColumnToValue[row].Add(name, null);
            }

            // Column-Row-Value
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            foreach (string row in _Rows)
            {
                dictionary.Add(row, null);
            }
            ColumnToRowToValue.Add(name, dictionary);

            // List
            _Columns.Add(name);
        }
        /// <summary>
        /// Get the column.
        /// </summary>
        /// <param name="name">Name of the column to get.</param>
        /// <returns>Row-to-value dictionary.</returns>
        public Dictionary<string, string> Column(string name)
        {
            if (!ColumnToRowToValue.TryGetValue(name, out Dictionary<string, string> rowToValue))
            {
                throw new Exception($"Column {name} does not exists.");
            }
            return rowToValue;
        }
        /// <summary>
        /// Remove a column.
        /// </summary>
        /// <param name="name">Name of the column to remove.</param>
        public void ColumnRemove(string name)
        {
            if (!ColumnToRowToValue.ContainsKey(name))
            {
                return;
            }

            ColumnToRowToValue.Remove(name);
            foreach (string row in _Rows)
            {
                RowToColumnToValue[row].Remove(name);
            }

            _Columns.Remove(name);
        }
        /// <summary>
        /// Check if the row exists.
        /// </summary>
        /// <param name="name">Name of the row.</param>
        /// <returns>If the row with specified name exists.</returns>
        public bool RowExists(string name)
        {
            return RowToColumnToValue.ContainsKey(name);
        }
        /// <summary>
        /// Add a row.
        /// </summary>
        /// <param name="name">Name of the row to add.</param>
        public void RowAdd(string name)
        {
            if (RowToColumnToValue.ContainsKey(name))
            {
                return;
            }

            // Column-Row-Value
            foreach (string column in _Columns)
            {
                ColumnToRowToValue[column].Add(name, null);
            }

            // Row-Column-Value
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            foreach (string column in _Columns)
            {
                dictionary.Add(column, null);
            }
            RowToColumnToValue.Add(name, dictionary);

            // List
            _Rows.Add(name);
        }
        /// <summary>
        /// Get the row.
        /// </summary>
        /// <param name="name">Name of the row to get.</param>
        /// <returns>Column-to-value dictionary.</returns>
        public Dictionary<string, string> Row(string name)
        {
            if (!RowToColumnToValue.TryGetValue(name, out Dictionary<string, string> columnToValue))
            {
                throw new Exception($"Row {name} does not exists.");
            }
            return columnToValue;
        }
        /// <summary>
        /// Remove the row.
        /// </summary>
        /// <param name="name">Name of the row to remove.</param>
        public void RowRemove(string name)
        {
            if (!_Rows.Contains(name))
            {
                return;
            }

            RowToColumnToValue.Remove(name);
            foreach (string column in _Columns)
            {
                ColumnToRowToValue[column].Remove(name);
            }

            _Rows.Remove(name);
        }
        #endregion

        #region Merge
        /// <summary>
        /// Merge multiple Csvs into a new one.
        /// </summary>
        /// <param name="csvs">Csvs to merge into a new one.</param>
        /// <returns>New Csv</returns>
        public static Csv Merge(params Csv[] csvs)
        {
            return Merge(OnCollisionBehavior.OVERWRITE, csvs);
        }
        /// <summary>
        /// Merge multiple Csvs into a new one.
        /// </summary>
        /// <param name="csvs">Csvs to merge into a new one.</param>
        /// <returns>New Csv</returns>
        public static Csv Merge(IEnumerable<Csv> csvs)
        {
            return Merge(OnCollisionBehavior.OVERWRITE, csvs);
        }
        /// <summary>
        /// Merge multiple Csvs into a new one.
        /// </summary>
        /// <param name="onCollisionBehavior">What to do if a duplicate row is found</param>
        /// <param name="csvs">Csvs to merge into a new one.</param>
        /// <returns></returns>
        public static Csv Merge(OnCollisionBehavior onCollisionBehavior, params Csv[] csvs)
        {
            return Merge(onCollisionBehavior, csvs);
        }
        /// <summary>
        /// Merge multiple Csvs into a new one.
        /// </summary>
        /// <param name="onCollisionBehavior">What to do if a duplicate row is found</param>
        /// <param name="csvs">Csvs to merge into a new one.</param>
        /// <returns></returns>
        public static Csv Merge(OnCollisionBehavior onCollisionBehavior, IEnumerable<Csv> csvs)
        {
            if (csvs == null)
            {
                throw new ArgumentNullException(nameof(csvs), "Csv list cannot be null.");
            }
            if (csvs.Count() < 2)
            {
                throw new ArgumentException("Csv list has to contain at least two items.", nameof(csvs));
            }

            Dictionary<string, Dictionary<string, string>> dictionary = new Dictionary<string, Dictionary<string, string>>();

            // Run through every csv
            foreach (Csv csv in csvs)
            {
                // Through every dictionary
                foreach (KeyValuePair<string, Dictionary<string, string>> keyAndDictionary in csv.ColumnToRowToValue)
                {
                    // Maybe key is not defined yet, then just copy and go on
                    if (!dictionary.ContainsKey(keyAndDictionary.Key))
                    {
                        dictionary.Add(keyAndDictionary.Key, new Dictionary<string, string>(keyAndDictionary.Value));
                        continue;
                    }
                    // Key already exists. Merge dictionaries under that key
                    foreach (KeyValuePair<string, string> variableAndValue in keyAndDictionary.Value)
                    {
                        // Check if variable is already defined
                        if (dictionary[keyAndDictionary.Key].ContainsKey(variableAndValue.Key))
                        {
                            switch (onCollisionBehavior)
                            {
                                case OnCollisionBehavior.KEEP:
                                    break;
                                case OnCollisionBehavior.OVERWRITE:
                                    dictionary[keyAndDictionary.Key][variableAndValue.Key] = variableAndValue.Value;
                                    break;
                                case OnCollisionBehavior.PANIC:
                                    throw new Exception($"Variable {variableAndValue.Key} is already defined.");
                                default:
                                    throw new Exception($"Merge way {onCollisionBehavior} is not supported.");
                            }
                        }
                        // Add a new variable under the specified key
                        dictionary[keyAndDictionary.Key].Add(variableAndValue.Key, variableAndValue.Value);
                    }
                }
            }

            Csv csverFirst = csvs.First();
            Csv merge = new Csv(csverFirst);
            merge.Read(dictionary);

            return merge;
        }
        #endregion

        #region Helpers
        /// <summary>
        /// Splits the text by new lines and then by commas.
        /// </summary>
        /// <param name="text">Text to split.</param>
        /// <returns></returns>
        private static List<List<string>> Linize(string text)
        {
            if (text == null)
            {
                return null;
            }
            if (text.Length == 0)
            {
                return new List<List<string>>();
            }

            List<List<string>> lines = new List<List<string>>();
            List<string> line = new List<string>();

            // State flags
            bool isInsideElement = false;
            bool isElementQuoteless = false;
            bool isElementCollected = false;
            //bool isSplitFound = true;
            //bool isFirstLine = true;
            //int firstLineElementsCount = -1;

            StringBuilder stringBuilder = new StringBuilder();
            char[] whitespaces = new char[] { ' ', '\t', '\r', '\n' };
            char[] delimiters = new char[] { ',' };
            int linesCount = 0;
            int newLineIndex = 0;
            int startIndex = 0;

            // UTF-16 Byte Order Mark for big-endian
            if (text[0] == 0xFEFF)
            {
                startIndex++;
            }
            // UTF-16 Byte Order Mark for little-endian
            else if (text[0] == 0xFFFE)
            {
                startIndex++;
            }

            for (int index = startIndex; index < text.Length; index++)
            {
                char charThis = text[index];
                char charNext = index < text.Length - 1 ? text[index + 1] : '\0';

                if (isInsideElement)
                {
                    if (isElementQuoteless)
                    {
                        if (charThis == '\n')
                        {
                            line.Add(stringBuilder.ToString().Trim(whitespaces));
                            lines.Add(line);
                            line = new List<string>();
                            stringBuilder.Clear();
                            isElementQuoteless = false;
                            isInsideElement = false;
                            newLineIndex = index;
                            linesCount++;
                        }
                        else if (delimiters.Contains(charThis))
                        {
                            line.Add(stringBuilder.ToString().Trim(whitespaces));
                            stringBuilder.Clear();
                            isElementQuoteless = false;
                            isInsideElement = false;
                        }
                        else
                        {
                            stringBuilder.Append(charThis);
                        }
                    }
                    // Quoted element
                    else
                    {
                        if (charThis == '"')
                        {
                            // Escaped double quotes
                            if (charNext == '"')
                            {
                                stringBuilder.Append(charThis);
                                index++;
                            }
                            // End of the element
                            else
                            {
                                line.Add(stringBuilder.ToString());
                                stringBuilder.Clear();
                                isElementCollected = true;
                                isInsideElement = false;
                            }
                        }
                        // A non-quote symbol
                        else
                        {
                            stringBuilder.Append(charThis);
                        }
                    }
                }
                else
                {
                    // Start of an element
                    if (charThis == '"')
                    {
                        // If an element has been collected recently and no delimiter has passed yet
                        if (isElementCollected)
                        {
                            throw new Exception($"Unexpected quote on line {linesCount + 1}, position {index - (newLineIndex + 1)}.");
                        }
                        isInsideElement = true;
                    }
                    // Comment
                    else if (charThis == '/' && charNext == '/')
                    {
                        if (!isElementCollected && stringBuilder.Length > 0)
                        {
                            line.Add(stringBuilder.ToString());
                            stringBuilder.Clear();
                        }
                        isElementCollected = false;
                        if (line.Count > 0)
                        {
                            lines.Add(line);
                            line = new List<string>();
                        }
                        while (index < text.Length)
                        {
                            charThis = text[index];
                            charNext = (index < text.Length - 1) ? text[index + 1] : '\0';
                            if (charThis == '\n')
                            {
                                break;
                            }
                            index++;
                        }
                        newLineIndex = index;
                        linesCount++;
                    }
                    else if (delimiters.Contains(charThis))
                    {
                        if (!isElementCollected)
                        {
                            line.Add(null);
                        }
                        isElementCollected = false;
                    }
                    else if (whitespaces.Contains(charThis))
                    {
                        if (charThis == '\n')
                        {
                            if (!isElementCollected && line.Count > 0)
                            {
                                line.Add(null);
                            }
                            if (line.Count > 0)
                            {
                                lines.Add(line);
                                line = new List<string>();
                            }
                            isElementCollected = false;
                            newLineIndex = index;
                            linesCount++;
                        }
                    }
                    // Start of a quoteless element
                    else
                    {
                        stringBuilder.Append(charThis);
                        isInsideElement = true;
                        isElementQuoteless = true;
                    }
                }

                //if (isInsideElement && isQuotelessElement)
                //{
                //    if (delimiters.Contains(charThis))
                //    {
                //        line.Add(stringBuilder.ToString().Trim(whitespaces));
                //        stringBuilder.Clear();
                //        isSplitFound = true;
                //        isInsideElement = false;
                //        isQuotelessElement = false;
                //    }
                //    else
                //    {
                //        stringBuilder.Append(charThis);
                //    }
                //}
                //else if (charThis == '"')
                //{
                //    // Switching in/out of an element
                //    if (charNext != '"')
                //    {
                //        // If no split was found before the element
                //        if (!isInsideElement && !isSplitFound)
                //        {
                //            throw new Exception($"No split between elements was found on the line {newLineIndex}.");
                //        }
                //        // Step into/out
                //        isInsideElement = !isInsideElement;
                //        // Stepped out of the element
                //        if (!isInsideElement)
                //        {
                //            // Flush what's collected
                //            line.Add(stringBuilder.ToString());
                //            stringBuilder.Clear();
                //            isSplitFound = false;
                //        }
                //    }
                //    // Just an escaped double quote
                //    else
                //    {
                //        if (isInsideElement)
                //        {
                //            stringBuilder.Append('"');
                //            index++;
                //        }
                //        // An empty element like in a sequence "abc","","","","def"
                //        else
                //        {
                //            line.Add("");
                //            isSplitFound = false;
                //            index++;
                //        }
                //    }
                //}
                //// A comment
                //else if (!isInsideElement && charThis == '/' && charNext == '/')
                //{
                //    while (index < text.Length)
                //    {
                //        charThis = text[index];
                //        charNext = (index < text.Length - 1) ? text[index + 1] : '\0';
                //        if (charThis == '\n')
                //        {
                //            break;
                //        }
                //        index++;
                //    }
                //    newLineIndex++;
                //}
                //// Outer delimiter symbols
                //else if (!isInsideElement)
                //{
                //    if (delimiters.Contains(charThis))
                //    {
                //        // In case of an ,,,-like sequence it is considered to be null elements, so add one
                //        if (isSplitFound)
                //        {
                //            line.Add(null);
                //        }
                //        else
                //        {
                //            isSplitFound = true;
                //        }
                //    }
                //    // Ignore white space characters
                //    else if (whitespaces.Contains(charThis))
                //    {
                //        ;
                //    }
                //    // Unexpected symbol outside of an element
                //    else
                //    {
                //        isInsideElement = true;
                //        isQuotelessElement = true;
                //        stringBuilder.Append(charThis);
                //        //throw new Exception($"A {charThis} symbol found outside of an element on the line {newLineIndex}.");
                //    }
                //}
                //// Any other character
                //else
                //{
                //    stringBuilder.Append(charThis);
                //}

                //// A new line counter
                //if (charThis == '\n')
                //{
                //    newLineIndex++;
                //    if (isFirstLine)
                //    {
                //        isFirstLine = false;
                //        firstLineElementsCount = line.Count;
                //    }
                //    if (isInsideElement && isQuotelessElement)
                //    {
                //        isInsideElement = false;
                //        isQuotelessElement = false;
                //        isSplitFound = true;
                //    }
                //    else if (!isInsideElement)
                //    {
                //        if (isSplitFound && line.Count != 0 /* If not an empty line */)
                //        {
                //            line.Add(null);
                //        }
                //        else
                //        {
                //            isSplitFound = true;
                //        }
                //    }
                //    if (line.Count != 0)
                //    {
                //        lines.Add(line);
                //        line = new List<string>();
                //    }
                //}
                //// Flush collected elemets
                //if (line.Count == firstLineElementsCount)
                //{
                //    lines.Add(line);
                //    line = new List<string>();
                //}
            }

            // Collect the last element
            if (isInsideElement)
            {
                if (isElementQuoteless)
                {
                    line.Add(stringBuilder.ToString().Trim(whitespaces));
                    lines.Add(line);
                    stringBuilder.Clear();
                }
                else
                {
                    throw new Exception("The last element of the last row does not have a closing quote.");
                }
            }
            else if (delimiters.Contains(text[text.Length - 1]))
            {
                if (!isElementCollected)
                {
                    line.Add(null);
                }
                isElementCollected = false;
            }

            // Collect the last line
            if (line.Count != 0)
            {
                lines.Add(line);
            }

            // Check all of the lines have the same count of elements
            for (int linesIndex = 0; linesIndex < lines.Count; linesIndex++)
            {
                if (lines[linesIndex].Count != lines[0].Count)
                {
                    string entry = lines[linesIndex].Count > 0 ? lines[linesIndex][0] : linesIndex.ToString();
                    throw new Exception($"Entry {entry} has {lines[linesIndex].Count} elements instead of {lines[0].Count}.");
                }
            }
            return lines;
        }

        //private string CsvParseColumnsReplace(string value)//, Dictionary<string, string> replaces)
        private static string Replace(string value, Tuple<string, string>[] replaces)
        {
            int start = 0;
            int end = value.Length;
            // Trim double quotes
            if (value.Length >= 2 && value[0] == '\"' && value[value.Length - 1] == '\"')
            {
                start++;
                end--;
            }

            StringBuilder stringBuilder = new StringBuilder();
            for (int valueIndex = start; valueIndex < end; valueIndex++)
            {
                bool isSubstituted = false;
                for (int replacesIndex = 0; replacesIndex < replaces.Length; replacesIndex++)
                {
                    string key = replaces[replacesIndex].Item1;
                    bool isMatched = true;
                    for (int keyIndex = 0; keyIndex < key.Length; keyIndex++)
                    {
                        if (valueIndex + keyIndex >= end)
                        {
                            isMatched = false;
                            break;
                        }
                        else if (value[valueIndex + keyIndex] == key[keyIndex])
                        {
                            continue;
                        }
                        else
                        {
                            isMatched = false;
                            break;
                        }
                    }
                    if (isMatched)
                    {
                        stringBuilder.Append(replaces[replacesIndex].Item2);
                        valueIndex += key.Length - 1 /* because of the increment in the for cycle */;
                        isSubstituted = true;
                        break;
                    }
                }
                if (!isSubstituted)
                {
                    stringBuilder.Append(value[valueIndex]);
                }
            }
            value = stringBuilder.ToString();

            //// Trim double quotes
            //if (value.Length >= 2 && value[0] == '\"' && value[value.Length - 1] == '\"')
            //{
            //    value = value.Substring(1, value.Length - 2);
            //}

            //// Replace double double quotes with single double quotes
            //value = value.Replace("\"\"", "\"");

            //// Deal with escapes
            //if (param_useEscapes != 0)
            //{
            //    bool escapeNewLines = (param_useEscapes & ReplaceCharacterEnum.NEW_LINE) != 0;
            //    bool escapeTabs = (param_useEscapes & ReplaceCharacterEnum.TAB) != 0;
            //    StringBuilder stringBuilder = new StringBuilder();
            //    for (int i = 0; i < value.Length; i++)
            //    {
            //        // Slash
            //        if (value[i] == '\\' && i < value.Length - 1)
            //        {
            //            // Double slash is an escaped slash
            //            if (escapeNewLines && value[i + 1] == 'n')
            //            {
            //                stringBuilder.Append("\n");
            //                i++;
            //            }
            //            else if (escapeNewLines && value[i + 1] == 'r')
            //            {
            //                stringBuilder.Append("\r");
            //                i++;
            //            }
            //            else if (escapeTabs && value[i + 1] == 't')
            //            {
            //                stringBuilder.Append("\t");
            //                i++;
            //            }
            //        }
            //        // Otherwise
            //        else
            //        {
            //            stringBuilder.Append(value[i]);
            //        }
            //    }
            //    value = stringBuilder.ToString();
            //}
            // Trim tabs and spaces
            //value = value.Trim(new char[] { ' ', '\t' });

            return value;
        }
        private static Dictionary<string, string> Invert(Dictionary<string, string> dictionary)
        {
            Dictionary<string, string> inverted = new Dictionary<string, string>();
            foreach (var pair in dictionary)
            {
                if (inverted.TryGetValue(pair.Value, out string key))
                {
                    continue;
                    //throw new Exception($"Cannot invert dictionary because value of {pair.Value} is assigned to both keys {key} and {pair.Key}.");
                }
                inverted[pair.Value] = pair.Key;
            }
            return inverted;
        }
        //private static bool TryGetKeys(Dictionary<string, string> dictionary, string value, out List<string> keys)
        //{
        //    keys = new List<string>();
        //    if (value == null)
        //    {
        //        foreach (var pair in dictionary)
        //        {
        //            if (pair.Value == null)
        //            {
        //                keys.Add(pair.Key);
        //            }
        //        }
        //    }
        //    else
        //    {
        //        foreach (var pair in dictionary)
        //        {
        //            if (value.Equals(pair.Value))
        //            {
        //                keys.Add(pair.Key);
        //            }
        //        }
        //    }
        //    return keys.Count > 0;
        //}
        #endregion
    }
}
